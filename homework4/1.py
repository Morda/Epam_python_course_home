#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 12 20:42:10 2018

@author: morda
"""
import string

numbers = string.digits

def to_digit(string, num=0, mult=1):
    if (not string and mult != 1): 
        return num / 2 if (num % 2 == 0) else num * 3 + 1
    if string[-1] in numbers: 
        return to_digit(string[:-1],
                        num + numbers.index(string[-1]) * mult,
                        mult * 10)
    else: return

while True:
    input_string = input()
    num = to_digit(input_string)

    if num: 
        print(num)
    elif input_string == 'cancel':
        break
    else: 
        print("Не удалось преобразовать введенный текст в число")
