#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 12 23:00:58 2018

@author: morda
"""
def make_cache(buff):
    k = buff
    def make(function):
        cache = {}
        def wrapper(*args):
            nonlocal k, cache
            try:
                if k > 0:
                    if args in cache:
                        k -= 1  
                        print (k)
                        return cache[args]
                    else:
                        val = function(*args)
                        cache[args] = val
                        return val
                else:
                    k = buff
                    cache = {}
            except:
                print('Cache limmit access')
        return wrapper
    return make

@make_cache(30)
def fib(n):
    if n < 2 : return n
    return fib(n-1) + fib(n-2)
