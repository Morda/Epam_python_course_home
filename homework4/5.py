#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 00:22:55 2018

@author: morda
"""
from __future__ import division
import math
import time

def stat(func):
    counter = 0
    time_counter = 0
    
    def wrapper(*args):
        nonlocal counter, time_counter
        start = time.time()
        counter += 1
        f = func(*args)
        time_counter += time.time() - start
        print('counter: ' + str(counter) + '\n' +'time: '+str(time_counter))    
        return f
        
    return wrapper

@stat
def fib0(n):
    if n < 2 : return n
    return fib0(n-1) + fib0(n-2)


@stat
def fib1(n):
    SQRT5 = math.sqrt(5)
    PHI = (SQRT5 + 1) / 2
    return int(PHI ** n / SQRT5 + 0.5)

@stat
def fib2(n):
    a = 0
    b = 1
    for __ in range(n):
        a, b = b, a + b
    return a


M = {0: 0, 1: 1}
@stat
def fib3(n):
    if n in M:
        return M[n]
    M[n] = fib3(n - 1) + fib3(n - 2)
    return M[n]








######################
    

def pow(x, n, I, mult):
    """
    Возвращает x в степени n. Предполагает, что I – это единичная матрица, которая 
    перемножается с mult, а n – положительное целое
    """
    if n == 0:
        return I
    elif n == 1:
        return x
    else:
        y = pow(x, n // 2, I, mult)
        y = mult(y, y)
        if n % 2:
            y = mult(x, y)
        return y


def identity_matrix(n):
    """Возвращает единичную матрицу n на n"""
    r = list(range(n))
    return [[1 if i == j else 0 for i in r] for j in r]


def matrix_multiply(A, B):
    BT = list(zip(*B))
    return [[sum(a * b
                 for a, b in zip(row_a, col_b))
            for col_b in BT]
            for row_a in A]

@stat
def fib4(n):
    F = pow([[1, 1], [1, 0]], n, identity_matrix(2), matrix_multiply)
    return F[0][1]