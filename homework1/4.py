#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 15:21:44 2018

@author: morda
"""
while True:
    string = input()
    if string == 'cancel':
        break
    lst = set(string.split())
    
    for i in range(1, len(lst)+2):
        if str(i) not in lst:
            print(i)
            break
        
# Сложность О(n), где n - длина входной строки. Время линейно.
