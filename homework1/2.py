#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 13:27:23 2018

@author: morda
"""
import operator

while True:
    text = input()
    if text == 'cancel':
        print ('Bye!')
        break
    lst = text.lower().split()
    d = {}
    
    for i in lst:
        d[i] = d.get(i, 0) + 1
        
    maxword = max(d.items(), key=operator.itemgetter(1))[0]
    
    for i in d:
        if d[maxword] == d[i]:
            print(str(d[i]) + ' - ' + i)
