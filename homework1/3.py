#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 13:43:43 2018

@author: morda
"""

import re

while True:
    s = input()
    if s == 'cancel':
        break
    k = re.findall(r"\-?[0-9]+", s)
    Sum = 0
    for i in k:
        Sum += int(i)
    print(Sum)