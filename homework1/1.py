#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 21:59:48 2018

@author: morda
"""

string = input()
lst = list(set(string.split()))
s = ''
for item in lst:
    s += (item + ' ')
print(s)