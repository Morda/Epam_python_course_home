#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 16:11:08 2018

@author: morda
"""

s = 0
for i in range(1000000):
    if (str(i) == str(i)[::-1]) and (bin(i)[2:] == bin(i)[-1:1:-1]):
        s += i
print(s)