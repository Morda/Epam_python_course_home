#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 22:28:51 2018

@author: morda
"""

def atom (value=None):
    
    def get_value():
        nonlocal value
        return value
    
    def set_value(set_value):
        nonlocal value
        value = set_value
        return value
    
    def process_value(*args):
        nonlocal value
        for f in args:
            value = f(value)     
        return value
    
    return get_value, set_value, process_value