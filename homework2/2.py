#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 08:04:41 2018

@author: morda
"""

counter_name = 0
count = 0

def make_it_count(func, counter):
    def wrapper(*args, **kwargs): 
        nonlocal counter
        globals()[counter] += 1
        counter_name
        return func(*args, **kwargs)
    return wrapper


def s(a):
    print(a)
    return a
