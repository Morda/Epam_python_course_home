#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 12:23:32 2018

@author: morda
"""

def letters_range(*args):
    alfabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n',
               'o','p','q','r','s','t','u','v','w','x','y','z']
    
    if len(args) == 2:
        start_ind = alfabet.index(args[0])
        stop_ind = alfabet.index(args[1])
        step = 1
    elif len(args) == 3:
        start_ind = alfabet.index(args[0])
        stop_ind = alfabet.index(args[1])
        step = args[2]
    else:
        start_ind = 0
        stop_ind = alfabet.index(args[0])
        step = 1
        
    ans = ''
    
#    print (start_ind, stop_ind, step)
    
    for i in alfabet[start_ind:stop_ind:step]:
        ans += i
    return ans
