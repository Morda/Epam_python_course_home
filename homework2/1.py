#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 08:14:13 2018

@author: morda
"""

def partial(func, *fixated_args, **fixated_kwargs):
    
    def wrapper(*args, **kwargs):
        
        if len(fixated_args) > 0:
            args = tuple(list(args)+list(fixated_args))
        if len(fixated_kwargs) > 0:
            kwargs = tuple(list(kwargs)+list(fixated_kwargs))
        
        return func(args, kwargs)
    
    docstring = "A partial implementation of {0} \n with pre-applie arguements being: {1} ; {2}".format(func.__name__, fixated_args, fixated_kwargs)
    wrapper.__doc__ = docstring
    wrapper.__name__ = func.__name__
    
    return wrapper

def s (a,b):
    print(a, b)
    
