#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 13:03:50 2018

@author: morda
"""



def bfs(start, E):
    stack = [start]
    ans = []
    ans.append(start)
    while stack:
#        st = iter(stack)
        w = stack.pop(0)
#        w = next(st)
        for p in E[w]: 
            if p not in ans:
                ans.append(p)
                stack += E[p]
    return ans

E = {"A": ["B", "C"], "B": ["A", "C", "D", "E"], "C": ["A", "B", "E"], "D": ["B"], "E": ["B", "C"]}
start = "A"

print(bfs(start, E))

