#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 18:02:17 2018

@author: morda
"""

def merge_files(file_name1, file_name2):
    file = open('answer.txt', 'w')
    with open(file_name1) as f1:
        with open(file_name2) as f2:
            
            line1 = f1.readline()
            line2 = f2.readline()
                        
            while line1 and line2:
                if int(line1) > int(line2):
                    file.write(line2)
                    line2 = f2.readline()
                else:
                    file.write(line1)
                    line1 = f1.readline()
                    
            if line1:
                file.write('\n'+line1)
            if line2:
                file.write('\n'+line2)                
    file.close()