#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 23:42:06 2018

@author: morda
"""

import os

#path = '/home/morda/PyProj'

def hardlink_check(path):
    tree = os.walk(path)
    for adress, dirs, files in tree:
        if files:
            mark = False
            for i in range(len(files)-1):
                for j in range(i+1, len(files)):
                    wi = adress + '/' + files[i]
                    wj = adress + '/' + files[j]
                    if os.stat(wi).st_ino == os.stat(wj).st_ino and not mark:
                        mark = True
                        break
    return mark