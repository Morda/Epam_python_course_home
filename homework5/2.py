#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 19:30:37 2018

@author: morda
"""

def is_armstrong(number):
    s = sum(map(lambda x: int(x)**len(list(str(number))), list(str(number))))
    return s==number