#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 16:03:15 2018

@author: morda
"""

# problem 6 

ans6 = (sum([x for x in range(101)]))**2 - sum([x**2 for x in range(101)])

# problem 9

ans9 = [(x,y,z) for x in range(1,500) for y in range (x,500) for z in range(y, 500) if x**2 + y**2 == z**2 and x + y + z == 1000]

# problem 48

ans48 = str(sum([x**x for x in range(1,1000)]))[-10:]

# problem 40

from functools import reduce

testnumb = str('0.'+'2'*10**7)

ans40 = reduce(lambda a,b: a*b, [int(testnumb[int('1'+'0'*n) + 1]) for n in range(7)])