#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 19:41:32 2018

@author: morda
"""


#- берём любое натуральное число n, если оно чётное, то делим его на 2
#- если нечётное, то умножаем на 3 и прибавляем 1 (получаем 3n + 1)
#- над полученным числом выполняем те же самые действия, и так далее

def collarz_steps(n):
    s = []
    s.append(n)
    while s[-1] != 1:
        if not s[-1]%2:
            s.append(s[-1]/2)
        else:
            s.append(3*s[-1] + 1)
    return len(s)-1