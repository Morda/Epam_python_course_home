#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 11:47:56 2018

@author: morda

"""

from functools import reduce

s1 = sum([x**2 for x in range(10**3)])

s2 = sum(list(map(lambda x: x**2, range(10**3))))

s3 = reduce(lambda x, y: x + y, [x**2 for x in range(10**3)])
